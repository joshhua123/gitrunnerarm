FROM ubuntu
  
RUN apt-get update
RUN DEBIAN_FRONTEND="noninteractive" apt-get -y install tzdata
RUN apt-get -y install docker curl gitlab-runner
RUN id -u gitlab-runner &>/dev/null ||useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash 
  
RUN gitlab-runner register --non-interactive --url https://gitlab.com --registration-token 5QcxHexfqCCfkB3q6V4f \
  --executor docker --docker-image docker
 
ENTRYPOINT ["gitlab-runner", "start"] 